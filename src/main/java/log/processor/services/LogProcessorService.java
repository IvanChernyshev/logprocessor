package log.processor.services;

import log.processor.configurations.properties.ApplicationProperties;
import log.processor.services.handlers.ExecutorHandler;
import log.processor.services.handlers.FileHandler;
import log.processor.services.handlers.TextHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class LogProcessorService {

    private final ApplicationProperties applicationProperties;
    private final MessageAggregationService messageAggregationService;

    public LogProcessorService(ApplicationProperties applicationProperties,
                               MessageAggregationService messageAggregationService) {
        this.applicationProperties = applicationProperties;
        this.messageAggregationService = messageAggregationService;
    }

    @PostConstruct
    public void process() {
        final List<Path> logPaths = FileHandler.findPath(applicationProperties.getFiles().getPath());
        if (!logPaths.isEmpty()) {
            final ExecutorService executorService = Executors.newCachedThreadPool();
            final List<Callable<String>> tasks = new ArrayList<>();
            logPaths.forEach(path -> tasks.add(() -> messageAggregationService.processMessage(FileHandler.readFromFile(path))));
            try {
                executorService.invokeAll(tasks);
                FileHandler.writeToFile(applicationProperties.getFiles().getPath() + "/report.txt",
                        TextHandler.prepareReportFromMap(messageAggregationService.getLogMessages(),
                                applicationProperties.getFiles().getLogLevel()));
                log.info("Processing log messages finished with success");
            } catch (InterruptedException ex) {
                log.error("Unable to process logs aggregation", ex);
            } finally {
                messageAggregationService.clear();
                ExecutorHandler.shutdownExecutor(executorService);
            }
        }
    }
}
