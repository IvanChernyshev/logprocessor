package log.processor.services.handlers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorHandler {

    public static void shutdownExecutor(ExecutorService executorService) {
        final long executorShutdownTimeoutSec = 10;
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(executorShutdownTimeoutSec, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            executorService.shutdownNow();
        }
    }
}
