package log.processor.services.handlers;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Map.Entry.comparingByKey;

public class TextHandler {

    private final static String REPORT_MESSAGE = " Number of log messages with status ";

    public static String prepareReportFromMap(Map<String, AtomicInteger> report, String address) {
        final StringBuilder builder = new StringBuilder();
        report.entrySet()
                .stream()
                .sorted(comparingByKey())
                .forEach((entry) -> builder.append(entry.getKey())
                        .append(REPORT_MESSAGE)
                        .append(address)
                        .append(" ")
                        .append(entry.getValue())
                        .append("\n"));
        return builder.toString();
    }
}
