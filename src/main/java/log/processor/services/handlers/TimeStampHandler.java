package log.processor.services.handlers;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import static log.processor.utils.Const.EMPTY_STRING;

public class TimeStampHandler {

    private final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd, HH.mm");
    private final static SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH.mm");
    private static final String HOUR_RANGE = "HOUR";
    private static final String MINUTE_RANGE = "MIN";
    private static final String DEFAULT_RANGE = "DEFAULT";


    public static String buildAggregationKey(long timestamp, String range) {
        Calendar startTime = null;
        Calendar endTime = null;
        switch (range) {
            case HOUR_RANGE:
                startTime = roundTimestamp(timestamp, ChronoUnit.HOURS);
                endTime = findEndTime(startTime, Calendar.HOUR_OF_DAY, 1);
                break;
            case MINUTE_RANGE:
                startTime = roundTimestamp(timestamp, ChronoUnit.MINUTES);
                endTime = findEndTime(startTime, Calendar.MINUTE, 1);
                break;
            case DEFAULT_RANGE:
                startTime = roundTimestampToDefault(timestamp);
                endTime = findEndTime(startTime, Calendar.MINUTE, 10);
                break;
        }
        if (startTime == null) {
            return EMPTY_STRING;
        }
        return concatKey(startTime, endTime);
    }

    private static String concatKey(Calendar start, Calendar end) {
        return simpleDateFormat.format(start.getTime()) + "-" + simpleTimeFormat.format(end.getTime());
    }

    private static Calendar findEndTime(Calendar startTime,
                                        int timeUnit,
                                        int timeAmount) {
        final Calendar endTime = Calendar.getInstance();
        endTime.setTime(startTime.getTime());
        endTime.add(timeUnit, timeAmount);
        return endTime;
    }

    private static Calendar roundTimestampToDefault(long timestamp) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(timestamp));
        final int minutes = calendar.get(Calendar.MINUTE);
        calendar.set(Calendar.MINUTE, minutes / 10 * 10);
        return calendar;
    }

    private static Calendar roundTimestamp(long timestamp, ChronoUnit chronoUnit) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(Instant.ofEpochMilli(timestamp)
                .truncatedTo(chronoUnit)
                .toEpochMilli()));
        return calendar;
    }
}
