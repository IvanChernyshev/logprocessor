package log.processor.services.handlers;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static log.processor.utils.Const.EMPTY_STRING;

@Slf4j
public class FileHandler {

    public static List<Path> findPath(String path) {
        try (Stream<Path> paths = Files.walk(Paths.get(path))) {
            return paths
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            log.error("Unable to find path for file " + path, e);
        }
        return Collections.emptyList();
    }

    public static String readFromFile(Path path) {
        try {
            return Files.readString(path);
        } catch (IOException e) {
            log.error("Unable to read file from path " + path, e);
            return EMPTY_STRING;
        }
    }

    public static void writeToFile(String address, String message) {
        final Path path = Paths.get(address);
        try {
            final File file = new File(path.toUri());
            file.getParentFile().mkdirs();
            file.createNewFile();

            Files.write(path, message.getBytes());
        } catch (IOException e) {
            log.error("Unable to write message to file " + path, e);
        }
    }
}
