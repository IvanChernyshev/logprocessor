package log.processor.services;

import log.processor.configurations.properties.ApplicationProperties;
import log.processor.models.LogMessage;
import log.processor.services.handlers.ExecutorHandler;
import log.processor.services.handlers.TimeStampHandler;
import log.processor.utils.Const;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MessageAggregationService {

    private final static String TEXT_DELIMITER = "\n";
    private final static String STRING_DELIMITER = ";";
    private final String logLevel;
    private final String timeRange;
    @Getter
    private final ConcurrentHashMap<String, AtomicInteger> logMessages = new ConcurrentHashMap<>();
    private final ReentrantLock lock = new ReentrantLock();

    public MessageAggregationService(ApplicationProperties applicationProperties) {
        this.logLevel = applicationProperties.getFiles().getLogLevel();
        this.timeRange = applicationProperties.getFiles().getAggregation().getRange();
    }

    @PostConstruct
    public void validateValues() {
        if (!logLevel.equals(LogLevel.DEBUG.name())
                && !logLevel.equals(LogLevel.INFO.name())
                && !logLevel.equals(LogLevel.TRACE.name())
                && !logLevel.equals(LogLevel.WARN.name())
                && !logLevel.equals(LogLevel.ERROR.name()
        )) {
            throw new IllegalArgumentException("MessageAggregationService created with incorrect log level");
        }
        if (StringUtils.isBlank(timeRange)) {
            throw new IllegalArgumentException("MessageAggregationService created with incorrect time range");
        }
    }

    public String processMessage(String rawMessage) {
        final List<LogMessage> filteredMessages = parseMessages(rawMessage)
                .stream()
                .filter(message -> message.getLogLevel().equals(logLevel))
                .sorted((Comparator.comparingLong(LogMessage::getTimestamp)))
                .collect(Collectors.toList());
        if (!filteredMessages.isEmpty()) {
            final ExecutorService executorService = Executors.newCachedThreadPool();
            final List<Callable<String>> tasks = new ArrayList<>();
            filteredMessages.forEach(m -> tasks.add(() -> aggregateMessage(m)));
            try {
                executorService.invokeAll(tasks);
            } catch (InterruptedException e) {
                log.error("Unable to aggregate message: " + rawMessage, e);
            } finally {
                ExecutorHandler.shutdownExecutor(executorService);
            }
        }
        return Const.EMPTY_STRING;
    }

    public void clear() {
        logMessages.clear();
    }

    private List<LogMessage> parseMessages(String message) {
        final List<LogMessage> logMessages = new ArrayList<>();
        final String[] messageStrings = message.split(TEXT_DELIMITER);
        for (String mess : messageStrings) {
            final String[] words = mess.split(STRING_DELIMITER);
            final LogMessage logMessage = LogMessage.builder()
                    .timestamp(timestampToLong(words[0]))
                    .logLevel(words[1])
                    .message(words[2])
                    .build();
            logMessages.add(logMessage);
        }
        return logMessages;
    }

    private long timestampToLong(String timestampString) {
        return Timestamp.valueOf(timestampString).getTime();
    }

    private enum LogLevel {
        INFO, WARN, DEBUG, ERROR, TRACE
    }

    private String aggregateMessage(LogMessage logMessage) {
        try {
            lock.lock();
            final String aggregationKey = TimeStampHandler.buildAggregationKey(logMessage.getTimestamp(), timeRange);
            if (!StringUtils.isBlank(aggregationKey)) {
                final AtomicInteger result = logMessages.putIfAbsent(aggregationKey, new AtomicInteger(1));
                if (result != null) {
                    result.incrementAndGet();
                }
            }
        } finally {
            lock.unlock();
        }
        return Const.EMPTY_STRING;
    }
}

