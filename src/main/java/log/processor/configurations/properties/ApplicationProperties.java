package log.processor.configurations.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "processor")
@Data
public class ApplicationProperties {
    private Files files;

    @Data
    public static class Files {
        private Aggregation aggregation;
        private String path;
        private String logLevel;

        @Data
        public static class Aggregation {
            private String range;
        }
    }
}
